package com.attendance.attendance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Thread splash_screen = new Thread(){
        	public void run(){
        		try{
        			sleep(5000);
        		}
        		catch(Exception e){
        			e.printStackTrace();
        		}
        		finally{
        			startActivity(new Intent(getApplicationContext(), PinActivity.class));
        			finish();
        		}
        	}
		};
		splash_screen.start();
	}
}
