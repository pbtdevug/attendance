package com.attendance.attendance;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailsActivity extends Activity {
	private TextView date,site,guardID,guardName,guardShift,confNum;
	private TextView [] views = new TextView[6];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Intent intent = getIntent();
		String detailsKey = intent.getStringExtra(HistoryActivity.activityKey);
		Log.d("Details Key", detailsKey);
		try{
			String details = HistoryActivity.history.get(detailsKey);
		
			site = (TextView) findViewById(R.id.d_site);
			views[1] = site;
			guardShift = (TextView) findViewById(R.id.d_shift);
			views[4] = guardShift;
			guardName = (TextView) findViewById(R.id.d_name);
			views[3] = guardName;
			guardID = (TextView) findViewById(R.id.d_id);
			views[2] = guardID;
			date = (TextView) findViewById(R.id.d_date);
			views[0] = date;
			confNum = (TextView) findViewById(R.id.confNum);
			views[5] = confNum;
		
		
			try{
				ArrayList<String> dets = ManageDataUpdate.split(details, "*");
				/*site.setText(dets.get(2).toString());
				guardShift.setText(dets.get(5).toString());
				guardName.setText(dets.get(4).toString());
				guardID.setText(dets.get(3).toString());
				confNum.setText(dets.get(6).toString());
				date.setText(dets.get(1).toString());*/
				int size = dets.size();
				for(int i=1;i<size;i++){
					views[i-1].setText(dets.get(i));
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

}
