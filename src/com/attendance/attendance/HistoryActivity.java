package com.attendance.attendance;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HistoryActivity extends ListActivity {
	private static final String DEBURG_TAG = "HistoryActivity";
	private ArrayAdapter<String> adapter;
	public static final String activityKey = "com.android.aandm.HistoryActivity";
	public static Hashtable<String,String> history = new Hashtable<String,String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		// Show the Up button in the action bar.
		setupActionBar();
		
		String historyData = readFromFile("history.txt");
		ArrayList<String> list = ManageDataUpdate.split(historyData, "^");
		Log.d("DEBURG_TAG", "Hist Data: " + list);
		int size = list.size();
		Log.d("DEBURG_TAG", "Hist Siz:e " + size);
		String submissions[] = new String[size];
		ArrayList<String> filedsVector;
		for(int i=0;i<size;i++){
			String hist = list.get(i).toString();
			filedsVector = ManageDataUpdate.split(hist, "*");
			String extract = filedsVector.get(0);
			submissions[i] = extract;
			history.put(extract, hist);
			Log.d(DEBURG_TAG, "Hist = "+extract);
		}
		
		adapter = new ArrayAdapter<String>(this, R.layout.list_elements, R.id.item, submissions);
        setListAdapter(adapter);
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	private String readFromFile(String fileName) {
        String ret = "";
         
        try {
            InputStream inputStream = openFileInput(fileName);
             
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                 
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                 
                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.d("DEBURG_TAG", "File not found: " + e.toString());
        }
        catch (IOException e) {
            Log.d("DEBURG_TAG", "Can not read file: " + e.toString());
        }
        return ret;
    }
	
	@Override 
	public void onListItemClick(ListView l, View v, int position, long id) {
        // Insert desired behavior here.
		String selectedHist = (String)l.getItemAtPosition(position);
		Log.d("List Clicked", "Selected Item "+selectedHist);
		Intent intent = new Intent(this,DetailsActivity.class); 
		intent.putExtra(activityKey, selectedHist);
        startActivity(intent);
    }

}
