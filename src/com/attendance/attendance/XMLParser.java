package com.attendance.attendance;

import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.util.Log;
import android.util.Xml;

public class XMLParser {
	private static final String DEBUG_TAG = "XMLParser";
    private static final String ns = null;
    private static String dateTime;
    public static String signature = new String(),status,confirmationNum,trxnNum,guardName = "";
	
	public static String parseXML(InputStream is){//throws XmlPullParserException, IOException
        String data = "";
        status = null;confirmationNum=null;trxnNum=null;
		try{
        	XmlPullParser parser = Xml.newPullParser();
        	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        	parser.setInput(is, null);
        	parser.nextTag();
        	parser.require(XmlPullParser.START_TAG, ns, "reply");
        	
        	String  id = new String(),
        						phone = new String()
        						,action = new String(), site = new String(),
        						siteID = new String(),number = new String(),
        						officer = new String();
        	
        	while (parser.next() != XmlPullParser.END_TAG) {
        		if (parser.getEventType() != XmlPullParser.START_TAG) {
        			continue;
        		}
        		String name = parser.getName();
        		// Starts by looking for the update tag
        		if(name.equals("dateTime")){
        			parser.require(XmlPullParser.START_TAG, ns, "dateTime");
        			if (parser.next() == XmlPullParser.TEXT) {
        				dateTime = (parser.getText()).substring(0,10);
        				PinActivity.date = dateTime;
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "dateTime");
        			if(dateTime == null){
        				//dateTime = FormsActivity.getDateTime();
        			}
        			Log.d(DEBUG_TAG, "Date: "+dateTime);
        		}
        		else if(name.equals("signature")){
        			parser.require(XmlPullParser.START_TAG, ns, "signature");
        			if (parser.next() == XmlPullParser.TEXT) {
        				signature = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "signature");
        			if(signature == null){
        				signature = "null";
        			}
        			Log.d(DEBUG_TAG, "Signature: "+signature);
        		}
        		else if(name.equals("status")){
        			parser.require(XmlPullParser.START_TAG, ns, "status");
        			if (parser.next() == XmlPullParser.TEXT) {
        				status = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "status");
        			Log.d(DEBUG_TAG, "Status: "+status);
        		}
        		else if(name.equals("confNo")){
        			parser.require(XmlPullParser.START_TAG, ns, "confNo");
        			if (parser.next() == XmlPullParser.TEXT) {
        				confirmationNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "confNo");
        			Log.d(DEBUG_TAG, "ConfNum: "+confirmationNum);
        		}
        		else if(name.equals("transNo")){
        			parser.require(XmlPullParser.START_TAG, ns, "transNo");
        			if (parser.next() == XmlPullParser.TEXT) {
        				trxnNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "transNo");
        			Log.d(DEBUG_TAG, "TransNum: "+trxnNum);
        		}
        		else if (name.equals("update")) {
        			parser.require(XmlPullParser.START_TAG, ns, "update");
        			while (parser.next() != XmlPullParser.END_TAG) {
        		        
        				if (parser.getEventType() != XmlPullParser.START_TAG) {
        	                continue;
        	            }
        				String name1 = parser.getName();
        				if (name1.equals("record")) {
        					parser.require(XmlPullParser.START_TAG, ns, "record");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name2 = parser.getName();
        						if (name2.equals("id")) {
        							parser.require(XmlPullParser.START_TAG, ns, "id");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				id = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "id");
        		        			Log.d(DEBUG_TAG, "id: "+id);
        						}
        						else if(name2.equals("name")){
        							parser.require(XmlPullParser.START_TAG, ns, "name");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				guardName = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "name");
        		        			Log.d(DEBUG_TAG, "GuardName: "+guardName);
        						}
        						else if(name2.equals("number")){
        							parser.require(XmlPullParser.START_TAG, ns, "number");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				phone = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "number");
        		        			Log.d(DEBUG_TAG, "PhoneNumber: "+phone);
        						}
        						else if(name2.equals("action")){
        							parser.require(XmlPullParser.START_TAG, ns, "action");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				action = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "action");
        		        			Log.d(DEBUG_TAG, action);
        						}
        						else{
        							skip(parser);
        						}
        					}
        					if("INSERT".equalsIgnoreCase(action)){
                                if(id!=null&&guardName!=null){
                                	PinActivity.guardsHash.put(id.trim(),guardName.trim());
                                	//unitsHash.put(itemName.trim(), itemUnit);
                                }
                            }
                            else if("DELETE".equalsIgnoreCase(action)){
                            	PinActivity.guardsHash.remove(id.trim());
                            }
        					//items.add(itemName.trim());
        				}
        				else if (name1.equals("deployment")) {
        					parser.require(XmlPullParser.START_TAG, ns, "deployment");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name3 = parser.getName();
        						if (name3.equals("deploymentName")) {
        							parser.require(XmlPullParser.START_TAG, ns, "deploymentName");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				site = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "deploymentName");
        		        			Log.d(DEBUG_TAG, "Site: "+site);
        						}
        						else if(name3.equals("deploymentID")){
        							parser.require(XmlPullParser.START_TAG, ns, "deploymentID");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				siteID = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "deploymentID");
        		        			Log.d(DEBUG_TAG, "SiteID: "+siteID);
        						}
        						else if(name3.equals("deploymentOfficerName")){
        							parser.require(XmlPullParser.START_TAG, ns, "deploymentOfficerName");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				officer = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "deploymentOfficerName");
        		        			Log.d(DEBUG_TAG, "officer: "+officer);
        						}
        						else if(name3.equals("deploymentOfficerMobileNumber")){
        							parser.require(XmlPullParser.START_TAG, ns, "deploymentOfficerMobileNumber");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				Log.d(DEBUG_TAG, "in Action ");
        		        				number = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "deploymentOfficerMobileNumber");
        		        			Log.d(DEBUG_TAG, "Number: "+number);
        						}
        						else if(name3.equals("action")){
        							parser.require(XmlPullParser.START_TAG, ns, "action");
        		        			if (parser.next() == XmlPullParser.TEXT) {
        		        				Log.d(DEBUG_TAG, "in Action ");
        		        				action = parser.getText();
        		        		        parser.nextTag();
        		        		    }
        		        			parser.require(XmlPullParser.END_TAG, ns, "action");
        		        			Log.d(DEBUG_TAG, "Action: "+action);
        						}
        						else{
        							skip(parser);
        						}
        					}
        					if("INSERT".equalsIgnoreCase(action)){
                                if(siteID!=null||site!=null){
                                	PinActivity.sitesHash.put(siteID.trim(),siteID+"-"+site.trim());
                                }
                                
                            }
                            else if("DELETE".equalsIgnoreCase(action)){
                            	PinActivity.sitesHash.remove(siteID.trim());
                            }
        					//sites.add(site);
        				}
        				else {
                			skip(parser);
                		}
        			}
        		}
        		else {
        			skip(parser);
        		}
        	}
        	
        	if(signature.length()>0&&(id.length()>0||siteID.length()>0)){
        		data = signature+"*"+PinActivity.guardsHash+"*"+PinActivity.sitesHash;
        	}
        }
        catch(IOException ex){
        	ex.printStackTrace();
        }
        catch(XmlPullParserException ex){
        	ex.printStackTrace();
        }
        finally {
        	try{
            is.close();
            }
        	catch(IOException ex){
            	ex.printStackTrace();
            }
        }
        return data;
	}
	
	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
