package com.attendance.attendance;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preference);
    }
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onResume() {
        super.onResume();
        // Registers a callback to be invoked whenever a user changes a preference.
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onPause() {
        super.onPause();

        // Unregisters the listener set in onResume().
        // It's best practice to unregister listeners when your app isn't using them to cut down on
        // unnecessary system overhead. You do this in onPause().
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
	
	 @Override
	    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	        // Sets refreshDisplay to true so that when the user returns to the main
	        // activity, the display refreshes to reflect the new settings.
	    	PinActivity.refreshDisplay = true;
	 }

}
