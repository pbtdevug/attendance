package com.attendance.attendance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Timer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AttendanceActivity extends Activity implements OnItemSelectedListener{
	private Spinner sitesSpinner,shiftsSpinner;
	public static String selectedSite,selectedShift,guardID,siteID;
	//private Handler handler = new Handler(Looper.getMainLooper());
	//private static AttendanceActivity AttendanceActivity;
	public Timer timer;
	private static final String emptySite = "-------Select Site-------";
	private static final String emptyShift = "-------Select Shift-------";
	private static String DEBURG_TAG = "AttendanceActivity";
	private ArrayList<String> sites;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendance);
		createSpinners();
		
		//AttendanceActivity = this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.attendance, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent histIntent = new Intent(this, HistoryActivity.class);
	        startActivity(histIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void createSpinners(){
		//TextView tv = (TextView) findViewById(R.id.date);
		String date = PinActivity.date;
		if(date.length()>16){
			setTitle("Deployment "+date);
			//tv.setText(date.substring(0, 10));
		}
		else{
			setTitle("Deployment "+date);
			//tv.setText(date);
		}
		
		sitesSpinner = (Spinner) findViewById(R.id.siteSpinner);
		sitesSpinner.setOnItemSelectedListener(this);
		//get sites list from sites hashtable
		Collection<String> sitesList = PinActivity.sitesHash.values();
		sites = new ArrayList<String>(sitesList);
		Collections.sort(sites, String.CASE_INSENSITIVE_ORDER);
		//adapter for sites
		ArrayAdapter<String> sitesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,sites);
		sitesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sitesSpinner.setAdapter(sitesAdapter);
		
		shiftsSpinner = (Spinner) findViewById(R.id.shiftSpinner);
		shiftsSpinner.setOnItemSelectedListener(this);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> shiftsAdapter = ArrayAdapter.createFromResource(this,
				R.array.shift_spinner, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		shiftsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			// Apply the adapter to the spinner
		shiftsSpinner.setAdapter(shiftsAdapter);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
		if(parent.equals(sitesSpinner)){
			selectedSite = (String)parent.getItemAtPosition(pos);
			siteID = getsiteID();
		}
		else if(parent.equals(shiftsSpinner)){
			selectedShift = (String)parent.getItemAtPosition(pos);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void submitRecord(View view){
		EditText idTextField = (EditText) findViewById(R.id.guard_id);
		guardID = idTextField.getText().toString();
		idTextField.setText("");
		
		if(guardID.length()>2){
			if(!selectedSite.equals(emptySite)){
				if(!selectedShift.equals(emptyShift)){
					Intent summIntent = new Intent(this, SummaryActivity.class);
			        startActivity(summIntent);
				}
				else{
					Toast.makeText(AttendanceActivity.this,"No shift selected!", Toast.LENGTH_LONG).show();
				}
			}
			else{
				Toast.makeText(AttendanceActivity.this,"No site selected!", Toast.LENGTH_LONG).show();
			}
		}
		else{
			Toast.makeText(AttendanceActivity.this,"Guard ID must 4-6 characters", Toast.LENGTH_LONG).show();
		}
	}
	
	public String getsiteID(){
		String id = "";
		Enumeration<String> idEnum = PinActivity.sitesHash.keys();
		while(idEnum.hasMoreElements()){
			String key = idEnum.nextElement();
			String value = PinActivity.sitesHash.get(key);
			if(value.equals(selectedSite)){
				id = key;
				break;
			}
		}
		Log.d(DEBURG_TAG, "SiteID: "+id);
		return id;
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
        //watchDog();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    /*private void watchDog(){
    	timer = new Timer();
        try{
            //thread wakes up 1 min (60000) after the start of app to start sending pending transactions 
        	timer.schedule(new AttendanceActivity.KillTask(),1*60000);
            // every after 2 min (120000) one pending transaction is sent to the server until they are all done
        }
        catch(IllegalStateException ex){
        	
        }
        catch(IllegalArgumentException ex){
        	
        }
    }
    
    class  KillTask  extends TimerTask{
        public void run() {
        	AttendanceActivity.getInstance().finish();
        }
    }
    
    public AttendanceActivity getInstance(){
		return AttendanceActivity;
	}*/
}
