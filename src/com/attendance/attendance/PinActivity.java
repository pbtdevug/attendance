package com.attendance.attendance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimerTask;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;

public class PinActivity extends Activity {

	public static final String activityKey = "com.android.PinActivity";
	private static final String DEBUG_TAG = "PinActivity";
	private ManageDataUpdate mdu = new ManageDataUpdate(this);
	public static String pin;
	//private Timer pendingTimer;
	//public SendPending sp = new SendPending();
	private Handler handler = new Handler(Looper.getMainLooper());
	public static Hashtable<String,String> sitesHash = new Hashtable<String,String>();
	public static Hashtable<String,String> guardsHash = new Hashtable<String,String>();
	public static String pendingFile, date;
	public static final String store = "store.txt";
	private String phoneID,simSerialNumber,simNumber;
	
	private NetworkReceiver receiver;
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;

    // The user's current network preference setting.
    public static String sPref = null;
    
    public static boolean wifiConnected = false;
    // Whether there is a mobile connection.
    public static boolean mobileConnected = false;
    
    //public static final String url = "http://www.peakbw.com/cgi-bin/igms_demo/saracen.mod.test";
    public static final String url = "http://www.peakbw.com/cgi-bin/igms_demo/senaca.test";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin);
		
		// Register BroadcastReceiver to track connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkReceiver();
        this.registerReceiver(receiver, filter);
        
        date = getDateTime();
        //get phone unique identifier 
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        phoneID = telephonyManager.getDeviceId();
        
        TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simSerialNumber = telemamanger.getSimSerialNumber();
        simNumber = telemamanger.getLine1Number();
        
        Log.d(DEBUG_TAG, "Serial = "+simSerialNumber);
        Log.d(DEBUG_TAG, "SimNumber = "+simNumber);
        Log.d(DEBUG_TAG, "DeviceID = "+phoneID);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		// Gets the user's network preference settings
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Retrieves a string value for the preferences. The second parameter
        // is the default value to use if a preference value is not found.
        sPref = sharedPrefs.getString("listPref", "Any");
        updateConnectedFlags();
        
      	//start process for sending pending records
        //pendingSheduler();
      	//create and show pin dialogue box
      	createDialog();
      	
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.pin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
        case R.id.action_settings:
                Intent settingsActivity = new Intent(getBaseContext(), SettingsActivity.class);
                startActivity(settingsActivity);
                return true;
        default:
                return super.onOptionsItemSelected(item);
        }
	}
	
	@Override
    protected void onPause() {
        super.onPause();
      //time out the
        handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );
        // Another activity is taking focus (this activity is about to be "paused").
    }
	
	@Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }
	
	@SuppressLint("InflateParams")
	private void createDialog(){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.prompts, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final EditText userInput = (EditText) promptsView.findViewById(R.id.user);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	final String userpin = userInput.getText().toString();
			    	pin = userpin;
			    	String signature = readSavedData();
			    	if(pin.length()==4){
			    		if(signature.equals("0")){
			    			startUpdate(userpin,signature);
			    		}
			    		else{
			    			startAttendanceActivity();
			    		}
			    	}
			    	else{
			    		Toast.makeText(PinActivity.this,"PIN must be 4 digits!", Toast.LENGTH_LONG).show();
			    	}
			    }
			  }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  dialog.cancel();
				  }
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	private void startUpdate(String pin, String signature){
		setContentView(R.layout.activity_update);
		String request = createUpdateXML(pin, signature);
		mdu.setUpdateVariable(request);
		loadPage(url);
	}
	
	public void startDialog(View view){
		createDialog();
	}
	
	/*private void pendingSheduler() {
    	pendingTimer = new Timer();
        try{
            //thread wakes up 1 min (60000) after the start of app to start sending pending transactions 
        	pendingTimer.scheduleAtFixedRate(new PinActivity.SendPendingTask(), 60000,1*120000);
            // every after 2 min (120000) one pending transaction is sent to the server until they are all done
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }*/
	
	class  SendPendingTask  extends TimerTask{
        public void run() {
        	String [] pend = getSavedFies();
        	ArrayList<String> pendFiles = new ArrayList<String>();
        	for(int i=0;i<pend.length;i++){
        		String pendingFile = pend[i];
        		if(pend.length>0&&!pendingFile.equals("history.txt")&&!pendingFile.equals("pending.txt")&&!pendingFile.equals("store.txt")){
        			pendFiles.add(pendingFile);
        		}
        		Log.d(DEBUG_TAG, "pendingFile = "+pendingFile);
        		
        	}
        	
        	if(pendFiles.size()>0){
        		pendingFile = pendFiles.get(0);
        		Log.d(DEBUG_TAG, "SelectedFiles = "+pendingFile);
        		String pendingTrxns = readFromFile(pendingFile);
        		if(pendingTrxns.length()!=0){
        			new SubmitTask().execute(pendingTrxns);
        		}
        	}
        }
    }
	
	private class SubmitTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... xml) {
                return "";//sp.getXMLReply(xml[0]);
        }

        @Override
        protected void onPostExecute(String result) {
        	//Log.d(DEBUG_TAG, result);
        	checkFeedBack(result);
        }
    }
	
	private void checkFeedBack(String resp){
		if(!resp.equals("")&&resp.equals("SUCCESSFUL")){
			Log.d(DEBUG_TAG, resp);
			try {	
				String dir = getFilesDir().getAbsolutePath();
				File file = new File(dir,pendingFile);
				boolean deleted = file.delete();
				Log.v(DEBUG_TAG,"deleted: " + deleted);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
    }
	
	private String [] getSavedFies(){
    	return getApplicationContext().fileList();
    }
	
	private String readFromFile(String fileName) {
        String ret = "";
         
        try {
            InputStream inputStream = openFileInput(fileName);
             
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                 
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                 
                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.d(DEBUG_TAG, "File not found: " + e.toString());
        }
        catch (IOException e) {
            Log.d(DEBUG_TAG, "Can not read file: " + e.toString());
        }
        return ret;
    }
	
	public class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            // Checks the user prefs and the network connection. Based on the result, decides
            // whether
            // to refresh the display or keep the current display.
            // If the userpref is Wi-Fi only, checks to see if the device has a Wi-Fi connection.
            if (WIFI.equals(sPref) && networkInfo != null
                    && networkInfo.getType() == ConnectivityManager.TYPE_WIFI){
                // If device has its Wi-Fi connection, sets refreshDisplay
                // to true. This causes the display to be refreshed when the user
                // returns to the app.
                refreshDisplay = true;
                Toast.makeText(context, R.string.wifi_connected, Toast.LENGTH_SHORT).show();
                Log.d(DEBUG_TAG, "wifi active: ");
                // If the setting is ANY network and there is a network connection
                // (which by process of elimination would be mobile), sets refreshDisplay to true.
            } else if (ANY.equals(sPref) && networkInfo != null) {
                refreshDisplay = true;
                
                // Otherwise, the app can't download content--either because there is no network
                // connection (mobile or Wi-Fi), or because the pref setting is WIFI, and there
                // is no Wi-Fi connection.
                // Sets refreshDisplay to false.
            } else {
                refreshDisplay = false;
                Toast.makeText(context, R.string.lost_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }
	
	private void updateConnectedFlags() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }
        Log.d(DEBUG_TAG, ""+wifiConnected);
        Log.d(DEBUG_TAG, ""+mobileConnected);
    }
	
	private void writeToFile(String content){
		FileOutputStream outputStream;
		try {
			outputStream = openFileOutput("store.txt", Context.MODE_PRIVATE);
    		Log.d("Hudson Bogere", outputStream.toString());
			outputStream.write(content.getBytes());
			outputStream.close();
		} 
		catch (Exception e) {
			Log.d("MainActivity", "Error " + e.toString());
			e.printStackTrace();
		}
	}
	
	private String readSavedData(){
        String udateSignature="0";
        
        ArrayList<String> id_site,id_guard;
        Log.d("Decode", "In Store");
        
        //sitesHash.put("0", "-------Select Site-------");
        
        try{
        	String storeData = readFromFile(store);
        	Log.d("Decode", "Read File");
        	if(!storeData.equals("")){
        		ArrayList<String> storesRecords = ManageDataUpdate.split(storeData, "*");
        		Log.d("Decode", "store = "+storesRecords);
                udateSignature = storesRecords.get(0).toString();
                
                String sitesSet = storesRecords.get(2).toString();
                ArrayList<String> sites = ManageDataUpdate.split(sitesSet.substring(1, sitesSet.length()-1),",");
                
                for(int i=0;i<sites.size();i++){
                    id_site = ManageDataUpdate.split(sites.get(i).toString(),"=");
                    String site = id_site.get(1).toString().trim();
                    sitesHash.put(id_site.get(0).toString().trim(),site);
                    id_site.clear();
                }
                sites.clear();
                
                String guardsSet = storesRecords.get(1).toString();
                ArrayList<String> guards = ManageDataUpdate.split(guardsSet.substring(1, guardsSet.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //
                for(int i=0;i<guards.size();i++){
                    id_guard = ManageDataUpdate.split(guards.get(i).toString(),"=");
                    String guard = id_guard.get(1).toString().trim();
                    guardsHash.put(id_guard.get(0).toString().trim(),guard);
                    id_guard.clear();
                    //items.add(item);
                }
                guards.clear();
        	}
        }
        catch(Exception ex){
        	Log.d("Decode", "Error = "+ex.getMessage());
        }
        return udateSignature;
    }
	
	private void loadPage(String connURL) {
        if (((sPref.equals(ANY)) && (wifiConnected || mobileConnected))|| ((sPref.equals(WIFI)) && (wifiConnected))) {
            // AsyncTask subclass
            new DownloadXMLTask().execute(connURL);
            Log.d(DEBUG_TAG, connURL);
        } else {
            showErrorPage();
        }
        Log.d(DEBUG_TAG, "wifiConnected: "+wifiConnected);
        Log.d(DEBUG_TAG, "mobileConnected: "+mobileConnected);
        Log.d(DEBUG_TAG, "pref: "+sPref);
    }
	
	private void showErrorPage() {
        // The specified network connection is not available. Displays error message.
        WebView myWebView = new WebView(this);
        myWebView.loadData(getResources().getString(R.string.connection_error),"text/html", null);
        setContentView(myWebView);
    }
	
	private class DownloadXMLTask extends AsyncTask<String, Void, String> {
		String data = "";
		InputStream stream = null;
        @Override
        protected String doInBackground(String... urls) {
        	try {
        		stream = mdu.downloadUrl(urls[0]);
        		try	{
        			data = XMLParser.parseXML(stream);
        		}
        		catch(Exception ex){
        			ex.printStackTrace();
        		}
        	}
        	catch(IOException ex){
            	ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                	try{
                		stream.close();
                    }catch(IOException ex){
                    	
                    }
                }
            }
        	return data;
        }

        @Override
        protected void onPostExecute(String result) {
        	Log.d(DEBUG_TAG, result);
        	if(!result.equals("")){
        		writeToFile(result);
        	}
        	
        	startAttendanceActivity();
        }
    }
	
	private void startAttendanceActivity(){
		Intent attendanceIntent = new Intent(this,AttendanceActivity.class);
		startActivity(attendanceIntent);
		finish();
	}
	
	public static String getDateTime() {//Fri 29 Nov 2013
        ArrayList<String> dateInfo = ManageDataUpdate.split((new Date()).toString()," ");
        String txn = dateInfo.get(2).toString()+" "+dateInfo.get(1)+" "+dateInfo.get(5).toString();
        return txn;
    }
	
	private String createUpdateXML(String pin,String signature){
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<updateReq>");
        xmlStr.append("<phoneId>").append(phoneID).append("</phoneId>");
        xmlStr.append("<simNumber>").append(simNumber).append("</simNumber>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<signature>").append(signature).append("</signature>");
        xmlStr.append("<version>").append(R.string.version).append("</version>");
        xmlStr.append("</updateReq>");
        //System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
}
