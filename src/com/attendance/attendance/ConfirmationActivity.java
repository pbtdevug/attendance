package com.attendance.attendance;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class ConfirmationActivity extends Activity {
	public static final String DEBUG_TAG = "ConfirmationActivity";
	private static String xmlStr;
	private ManageDataUpdate mdu = new ManageDataUpdate(new PinActivity());
	public static final String activityKey ="com.attendance.ConfirmationActivity";
	private String commit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmation);
	}
	
	@Override
	public void onStart(){
		super.onStart();
        Log.d(DEBUG_TAG, "started Connection.....: ");
        Intent intent = getIntent();
        commit = "CHECK";
        xmlStr = intent.getStringExtra(SummaryActivity.activityKey);
        //Log.d(DEBUG_TAG, "Request:"+xmlStr);
        
        // Only loads the page if refreshDisplay is true. Otherwise, keeps previous
        // display. For example, if the user has set "Wi-Fi only" in prefs and the
        // device loses its Wi-Fi connection midway through the user using the app,
        // you don't want to refresh the display--this would force the display of
        // an error page instead of stackoverflow.com content.
        if (PinActivity.refreshDisplay) {
        	checkSchedule();
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.confirmation, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void loadPage(String connURL) {
		if (((PinActivity.sPref.equals(PinActivity.ANY)) && (PinActivity.wifiConnected || PinActivity.mobileConnected))|| ((PinActivity.sPref.equals(PinActivity.WIFI)) && (PinActivity.wifiConnected))) {
            // AsyncTask subclass
            new DownloadXMLTask().execute(connURL);
            Log.d(DEBUG_TAG, connURL);
        } else {
            showErrorPage();
        }
        Log.d(DEBUG_TAG, "wifiConnected: "+PinActivity.wifiConnected);
        Log.d(DEBUG_TAG, "mobileConnected: "+PinActivity.mobileConnected);
        Log.d(DEBUG_TAG, "pref: "+PinActivity.sPref);
    }
	
	private void showErrorPage() {
		WebView myWebView = new WebView(this);
        myWebView.loadData(getResources().getString(R.string.connection_error),"text/html", null);
        setContentView(myWebView);
    }
	
	private class DownloadXMLTask extends AsyncTask<String, Void, String> {
		String data = "";
		InputStream stream = null;
        @Override
        protected String doInBackground(String... urls) {
        	try {
        		stream = mdu.downloadUrl(urls[0]);
        		try	{
        			data = XMLParser.parseXML(stream);
        		}
        		catch(Exception ex){
        			ex.printStackTrace();
        		}
        	}
        	catch(IOException ex){
            	ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                	try{
                		stream.close();
                    }catch(IOException ex){
                    	
                    }
                }
            }
        	return data;
        }
        
        @Override
        protected void onPostExecute(String result) {
        	Log.d(DEBUG_TAG, "Update = "+result);
        	if(ManageDataUpdate.respCode==200){
        		String status = XMLParser.status;
        		String confNum = XMLParser.confirmationNum;
        		String trxnNum = XMLParser.trxnNum;
        		String name = XMLParser.guardName;
        		if(name.length()==0){name="Guard";}
        		Log.d(DEBUG_TAG, "state: "+status);
        		Log.d(DEBUG_TAG, "trxnNum: "+confNum);
        		if(status!=null){
        			Log.d(DEBUG_TAG, "Status: "+status);
        			if((trxnNum!=null||confNum!=null)&&commit.equals("COMMIT")){
        				if(status.equalsIgnoreCase("SUCCESSFUL")){
        					//update phone store in case of updates
                			if(!result.equals("")){
                				writeToFile("store.txt",result,Context.MODE_PRIVATE);
                			}
                			String history = PinActivity.date+"-"+XMLParser.guardName+"*"+PinActivity.date+
                					"*"+AttendanceActivity.selectedSite+"*"+AttendanceActivity.guardID+"*"+
        							name+"*"+AttendanceActivity.selectedShift+"*"+confNum+"^";
                			
                			writeToFile("history.txt",history,Context.MODE_APPEND);
                			
        					createResponseDialog(status,R.layout.success_prompt,R.id.success);
        				}
        				else{
        					createResponseDialog(status,R.layout.error_prompt,R.id.error);
        				}
        			}
        			else if((trxnNum!=null||confNum!=null)&&commit.equals("CHECK")){
        				//update phone store in case of updates
        				Log.d(DEBUG_TAG, "ConfNum: "+confNum);
            			if(!result.equals("")){
            				writeToFile("store.txt",result,Context.MODE_PRIVATE);
            			}
    					createDialog(status);
    				}
        			else{
        				Log.d(DEBUG_TAG, "confNum: "+confNum);
        			}
        		}
        	}
        	else{
        		createResponseDialog("Connection Timeout",R.layout.error_prompt,R.id.error);
        	}
        }
	}
	
	/*private void startAttendanceActivity(){
		Intent attIntent = new Intent(this, AttendanceActivity.class);
        startActivity(attIntent);
	}*/
	
	private void checkSchedule(){
		mdu.setUpdateVariable(xmlStr);
		Log.d(DEBUG_TAG, "loading page: ");
        loadPage(PinActivity.url);
	}
	
	private void writeToFile(String file,String content,int mode){
		FileOutputStream outputStream;
		try {
			outputStream = openFileOutput(file, mode);
    		Log.d(DEBUG_TAG, "FileName = "+file);
    		Log.d(DEBUG_TAG, "Content = "+content);
			outputStream.write(content.getBytes());
			outputStream.close();
		} 
		catch (Exception e) {
			Log.d(DEBUG_TAG, "Error " + e.toString());
			e.printStackTrace();
		}
	}
	
	private void createDialog(String status){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.confirmation_prompt, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView originalView = (TextView) promptsView.findViewById(R.id.orignal);
		Log.d(DEBUG_TAG, "Dialog " + status);
		originalView.setText(status);
		
		final TextView newView = (TextView) promptsView.findViewById(R.id.change);
		
		if(AttendanceActivity.selectedShift.equals("DAY")||AttendanceActivity.selectedShift.equals("NIGHT")||AttendanceActivity.selectedShift.equals("DAY+NIGHT")){
			newView.setText("ID:"+AttendanceActivity.guardID+"-"+SummaryActivity.guardName+"-"+AttendanceActivity.selectedShift+" shift at site:"+SummaryActivity.site);
        }
        else{
        	newView.setText("ID:"+AttendanceActivity.guardID+"-"+SummaryActivity.guardName+"-"+AttendanceActivity.selectedShift+" shift");
        }
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	String commitRequest = SummaryActivity.createXML("COMMIT");
			    	commit = "COMMIT";
			    	mdu.setUpdateVariable(commitRequest);
			        loadPage(PinActivity.url);
			    }
			  }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  dialog.cancel();
					  finish();
				  }
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	private void createResponseDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	finish();
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
}
